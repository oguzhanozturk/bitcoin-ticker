package com.loodos.bitcointicker.ui.home

import android.app.Activity
import android.content.Intent
import com.loodos.bitcointicker.R
import com.loodos.bitcointicker.base.view.BaseActivity
import com.loodos.bitcointicker.databinding.ActivityHomeBinding

class HomeActivity (
    override val layoutResourceId: Int = R.layout.activity_home,
    override val classTypeOfViewModel: Class<HomeViewModel> = HomeViewModel::class.java
) : BaseActivity<ActivityHomeBinding, HomeViewModel>() {


    companion object {
        fun start(activity: Activity) {
            activity.startActivity(Intent(activity, HomeActivity::class.java).apply {

            })
        }
    }

}