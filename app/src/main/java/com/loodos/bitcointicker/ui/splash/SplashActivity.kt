package com.loodos.bitcointicker.ui.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.loodos.bitcointicker.R
import com.loodos.bitcointicker.base.view.BaseActivity
import com.loodos.bitcointicker.databinding.ActivitySplashBinding
import com.loodos.bitcointicker.ui.home.HomeActivity
import com.loodos.bitcointicker.util.extension.observe
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class SplashActivity(
    override val layoutResourceId: Int = R.layout.activity_splash,
    override val classTypeOfViewModel: Class<SplashViewModel> = SplashViewModel::class.java
) : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    override fun initialize() {

        GlobalScope.launch {
            delay(1000)
            HomeActivity.start(this@SplashActivity)
        }
    }


}