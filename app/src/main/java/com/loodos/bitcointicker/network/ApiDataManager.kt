package com.loodos.bitcointicker.network

import com.loodos.bitcointicker.network.service.ApiService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 ** Created by oguzhanozturk on 10.09.2020.
 */
@Singleton
class ApiDataManager @Inject constructor(private val apiService: ApiService){


    fun <T> Single<T>.applySchedulers(): Single<T> {
        return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}