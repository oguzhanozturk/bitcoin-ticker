package com.loodos.bitcointicker.di.module

import dagger.Module

/**
 ** Created by oguzhanozturk on 10.09.2020.
 */

@Module(
    includes = [
        ViewModelModule::class
    ]
)
class AppModule {


}