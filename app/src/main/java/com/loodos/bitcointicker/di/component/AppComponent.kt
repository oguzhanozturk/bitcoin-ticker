package com.loodos.bitcointicker.di.component

import android.app.Application
import com.loodos.bitcointicker.LoodosApp
import com.loodos.bitcointicker.di.module.ActivityModule
import com.loodos.bitcointicker.di.module.AppModule
import com.loodos.bitcointicker.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 ** Created by oguzhanozturk on 10.09.2020.
 */

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        NetworkModule::class,
        ActivityModule::class,
        AppModule::class
    ]
)
interface AppComponent : AndroidInjector<LoodosApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: Application)
}