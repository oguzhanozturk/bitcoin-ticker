package com.loodos.bitcointicker.di.module

import com.loodos.bitcointicker.ui.home.HomeActivity
import com.loodos.bitcointicker.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 ** Created by oguzhanozturk on 10.09.2020.
 */

@Module
abstract class ActivityModule {


    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun bindHomeActivity(): HomeActivity

}