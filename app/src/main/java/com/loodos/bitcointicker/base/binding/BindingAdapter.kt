package com.loodos.bitcointicker.base.binding

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import com.loodos.bitcointicker.R
import com.loodos.bitcointicker.util.ImageLoaderUtiliy

@BindingAdapter("url")
fun loadImage(
    view: ImageView,
    imageUrl: String
) {
    ImageLoaderUtiliy.imageLoaderWithCacheAndPlaceHolder(
        view.context,
        imageUrl,
        view,
        R.mipmap.ic_launcher_round
    )
}

@BindingAdapter("layoutMarginEnd")
fun setLayoutMarginBottom(view: View, dimen: Float) {
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.rightMargin = dimen.toInt()
    view.layoutParams = layoutParams
}
