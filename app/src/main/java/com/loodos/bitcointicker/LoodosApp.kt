package com.loodos.bitcointicker

import com.loodos.bitcointicker.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 ** Created by oguzhanozturk on 10.09.2020.
 */
class LoodosApp :DaggerApplication(){

    override fun onCreate() {
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out LoodosApp> {
        return DaggerAppComponent.builder().application(this).build()
    }
}